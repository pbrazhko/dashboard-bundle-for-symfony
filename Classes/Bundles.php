<?php

namespace CMS\DashboardBundle\Classes;

use Symfony\Component\HttpKernel\Kernel;

/**
 * Description of Bundles
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class Bundles {
    
    private $kernel = null;
    
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }
    
    public function getBundles(){
        return $this->kernel->getBundles();
    }
    
    public function getBundle($name){
        return $this->kernel->getBundle($name, true);
    }
    
    public function getSupportedBundles(){
        $bundles = array();
        
        foreach ($this->getBundles() as $bundle){
            if (!method_exists($bundle, 'isEnabled') || !$bundle->isEnabled()){
                continue;
            }
            
            if (method_exists($bundle, 'getDescription')){
                $supportBundle = $bundle->getDescription();

                if (is_array(reset($supportBundle))){
                    foreach($supportBundle as $_supportBundle){
                        $bundles[$bundle->getName()][] = $_supportBundle;
                    }
                }
                else{
                    $bundles[$bundle->getName()][] =  $supportBundle;
                }
            }
        }
        
        return $bundles;
    }

}