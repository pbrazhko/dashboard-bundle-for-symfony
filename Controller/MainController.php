<?php

namespace CMS\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    public function indexAction()
    {
        return $this->render('DashboardBundle:Main:index.html.twig');
    }
}
