<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 06.08.14
 * Time: 21:28
 */

namespace CMS\DashboardBundle\Interfaces;


interface CMSBundleInterface {
    public function isEnabled();
    public function getDescription();
} 