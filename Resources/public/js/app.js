/**
 * Created by Pavel on 03.07.14.
 */

'use strict'

angular.module('Dashboard', ['Dashborad.Controllers'])
.config(['$interpolateProvider', function ($interpolateProvider){
        $interpolateProvider.startSymbol('[[').endSymbol(']]')
    }])