<?php
namespace CMS\DashboardBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of DashboardExtension
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class DashboardExtension extends \Twig_Extension{

    private $container = null;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('system_modules', [$this, 'systemModules'], ['is_safe' => ['html'], 'needs_environment' => true])
        );
    }
    
    public function systemModules(\Twig_Environment $environment){
        $security = $this->container->get('security.authorization_checker');

        if (!$security->isGranted('IS_AUTHENTICATED_FULLY')){
            return false;
        }

        $bundles = $this->container->get('dashboard.bundles')->getSupportedBundles();
        
        return $environment->render(
                'DashboardBundle:Extension:bundles.html.twig',
                array(
                    'bundles' => $bundles
                ));
    }
    
    public function getName() {
        return 'dashboard_extension';
    }
}